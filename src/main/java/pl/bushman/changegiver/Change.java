package pl.bushman.changegiver;

import java.util.EnumMap;
import java.util.Map;

public class Change {
    private final Map<MoneyNote, Integer> moneyNoteMap = new EnumMap<>(MoneyNote.class);

    private Change(Map<MoneyNote, Integer> moneyNotesTab) {
        moneyNoteMap.putAll(moneyNotesTab);
    }

    public int getAmount(MoneyNote moneyNote) {
        Integer amount = moneyNoteMap.get(moneyNote);
        return amount == null ? 0 : amount;
    }


    static class ChangeBuilder {
        private final Map<MoneyNote, Integer> moneyNoteMap = new EnumMap<>(MoneyNote.class);

        public ChangeBuilder() {
        }

        public ChangeBuilder withNotes(MoneyNote note, Integer amount) {
            if (amount < 0) {
                throw new IllegalArgumentException("amount must be > 0. Actual: " + amount);
            }
            moneyNoteMap.put(note, amount);
            return this;
        }

        public Change build() {
            return new Change(moneyNoteMap);
        }

    }

}
