package pl.bushman.changegiver;

import pl.bushman.changegiver.Change.ChangeBuilder;

import java.util.EnumMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static java.util.Comparator.comparingInt;
import static pl.bushman.changegiver.MoneyUnit.GR;
import static pl.bushman.changegiver.MoneyUnit.ZL;

public class ChangeGiver {

    private static final Map<MoneyUnit, Set<MoneyNote>> ORDERED_NOTES_BY_UNIT = new EnumMap<>(MoneyUnit.class);

    static {
        for (MoneyUnit unit : MoneyUnit.values()) {
            ORDERED_NOTES_BY_UNIT.put(unit, new TreeSet<>(comparingInt(note -> -note.amount)));
        }

        for (MoneyNote note : MoneyNote.values()) {
            ORDERED_NOTES_BY_UNIT.get(note.unit).add(note);
        }
    }


    public Change giveChange(MoneyAmount amountToPay, MoneyAmount amountGiven) {
        MoneyAmount changeAmount = amountGiven.minus(amountToPay);
        ChangeBuilder changeBuilder = new ChangeBuilder();

        addNotesOfUnitToChange(changeAmount.zl, ZL, changeBuilder);
        addNotesOfUnitToChange(changeAmount.gr, GR, changeBuilder);

        return changeBuilder.build();
    }

    private void addNotesOfUnitToChange(int changeAmount, MoneyUnit unit, ChangeBuilder changeBuilder) {
        for (MoneyNote note : ORDERED_NOTES_BY_UNIT.get(unit)) {
            changeBuilder.withNotes(note, changeAmount / note.amount);
            changeAmount = changeAmount % note.amount;
        }
    }
}
