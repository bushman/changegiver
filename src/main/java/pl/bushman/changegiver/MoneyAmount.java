package pl.bushman.changegiver;

public class MoneyAmount {
    final int zl;
    final int gr;

    public MoneyAmount(int zl, int gr) {
        if (zl < 0 || gr < 0) {
            throw new IllegalArgumentException("zl and gr must be > 0. Actual zl: " + zl + ", gr: " + gr);
        }
        this.zl = zl;
        this.gr = gr;
    }

    public MoneyAmount minus(MoneyAmount moneyAmount) {
        int resultZl = zl;
        int resultGr = gr;

        if (moneyAmount.gr > resultGr) {
            resultGr += 100;
            resultZl--;
        }

        return new MoneyAmount(resultZl - moneyAmount.zl, resultGr - moneyAmount.gr);
    }

}
