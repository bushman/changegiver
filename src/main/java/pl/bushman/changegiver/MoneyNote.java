package pl.bushman.changegiver;

import static pl.bushman.changegiver.MoneyUnit.GR;
import static pl.bushman.changegiver.MoneyUnit.ZL;

public enum MoneyNote {
    HUNDRED_ZL(100, ZL), FIFTY_ZL(50, ZL), TWENTY_ZL(20, ZL), TEN_ZL(10, ZL), FIVE_ZL(5, ZL), TWO_ZL(2, ZL), ONE_ZL(1, ZL),
    FIFTY_GR(50, GR), TWENTY_GR(20, GR), TEN_GR(10, GR), FIVE_GR(5, GR), TWO_GR(2, GR), ONE_GR(1, GR);

    int amount;
    MoneyUnit unit;

    MoneyNote(int amount, MoneyUnit unit) {
        this.amount = amount;
        this.unit = unit;
    }
}
