package pl.bushman.changegiver;


import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static pl.bushman.changegiver.MoneyNote.HUNDRED_ZL;

public class ChangeGiverTest {

    ChangeGiver changeGiver = new ChangeGiver();

    @ParameterizedTest
    @CsvSource({"123.37,123.37,0,0,0,0,0,0,0,0,0,0,0,0,0",
            "123.37,125.69,0,0,0,0,0,1,0,0,1,1,0,1,0",
            "124.79,125.29,0,0,0,0,0,0,0,1,0,0,0,0,0"})
    public void givesChange(String amountToPayStr,
                            String amountGivenStr,
                            int hundredsZl,
                            int fiftysZl,
                            int twentysZl,
                            int tensZl,
                            int fivesZl,
                            int twosZl,
                            int onesZl,
                            int fiftysGr,
                            int twentysGr,
                            int tensGr,
                            int fivesGr,
                            int twosGr,
                            int onesGr) {

        MoneyAmount amountToPay = parse(amountToPayStr);
        MoneyAmount amountGiven = parse(amountGivenStr);


        Change change = changeGiver.giveChange(amountToPay, amountGiven);


        assertEquals(hundredsZl, change.getAmount(HUNDRED_ZL));
        assertEquals(fiftysZl, change.getAmount(MoneyNote.FIFTY_ZL));
        assertEquals(twentysZl, change.getAmount(MoneyNote.TWENTY_ZL));
        assertEquals(tensZl, change.getAmount(MoneyNote.TEN_ZL));
        assertEquals(fivesZl, change.getAmount(MoneyNote.FIVE_ZL));
        assertEquals(twosZl, change.getAmount(MoneyNote.TWO_ZL));
        assertEquals(onesZl, change.getAmount(MoneyNote.ONE_ZL));
        assertEquals(fiftysGr, change.getAmount(MoneyNote.FIFTY_GR));
        assertEquals(twentysGr, change.getAmount(MoneyNote.TWENTY_GR));
        assertEquals(tensGr, change.getAmount(MoneyNote.TEN_GR));
        assertEquals(fivesGr, change.getAmount(MoneyNote.FIVE_GR));
        assertEquals(twosGr, change.getAmount(MoneyNote.TWO_GR));
        assertEquals(onesGr, change.getAmount(MoneyNote.ONE_GR));

    }

    private MoneyAmount parse(String moneyStr) {
        String[] split = moneyStr.split("\\.");
        return new MoneyAmount(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
    }


}
