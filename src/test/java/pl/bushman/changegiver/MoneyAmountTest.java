package pl.bushman.changegiver;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

//TODO: params
class MoneyAmountTest {

    @Test
    void minus() {
        MoneyAmount result = new MoneyAmount(123, 67).minus(new MoneyAmount(52, 90));

        assertEquals(70, result.zl);
        assertEquals(77, result.gr);
    }

    @Test
    void throwsExceptionWhenNegativeAmounts() {
        assertThrows(IllegalArgumentException.class, () ->
                new MoneyAmount(123, -67)
        );
    }

    @Test
    void throwsExceptionWhenMinusAndNegativeResult() {
        assertThrows(IllegalArgumentException.class, () ->
                new MoneyAmount(123, 67).minus(new MoneyAmount(152, 90))
        );
    }
}