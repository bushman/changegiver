package pl.bushman.changegiver;

import org.junit.jupiter.api.Test;
import pl.bushman.changegiver.Change.ChangeBuilder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static pl.bushman.changegiver.MoneyNote.*;

class ChangeTest {

    @Test
    void getsAmount() {
        Change change = new ChangeBuilder().withNotes(ONE_ZL, 1).withNotes(FIFTY_GR, 2).build();

        assertEquals(1, change.getAmount(ONE_ZL));
        assertEquals(2, change.getAmount(FIFTY_GR));
        assertEquals(0, change.getAmount(HUNDRED_ZL));
    }

    @Test
    void throwsExceptionWhenNegativeAmount() {
        assertThrows(IllegalArgumentException.class, () ->
                new ChangeBuilder().withNotes(ONE_ZL, -1)
        );
    }
}